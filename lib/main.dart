import 'package:carbon_emission/screens/login.dart';

import 'package:carbon_emission/screens/splashScreen_2.dart';
import 'package:flutter/material.dart';

import './screens/homeScreen.dart';

import './screens/splashScreen_1.dart';

import 'package:flutter/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  bool loggedIN = false;
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Carbon Hero',
      theme: ThemeData(
        primaryColor: Colors.white,
        accentColor: Colors.black,
      ),
      home: loggedIN == true ? HomeScreen() : SplashScreen_1(),
      routes: {
        HomeScreen.routeName: (ctx) => HomeScreen(),
        SplashScreen_1.routeName: (ctx) => SplashScreen_1(),
        SplashScreen_2.routeName: (ctx) => SplashScreen_2(),
        LogIn.routeName: (ctx) => LogIn(),
      },
    );
  }
}
